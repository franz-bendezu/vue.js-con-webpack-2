import VueRouter from 'vue-router';
import principal from '../view/principal.vue'
import nosotros from '../view/nosotros.vue'
import contactos from '../view/contactos.vue'
import vue from 'vue';

vue.use(VueRouter);

export default new VueRouter({
    routes:[
        {
            path: '/',
            name: '',
            component: principal,
        },
        {
            path: '/nosotros',
            name: '',
            component: nosotros,
        },
        {
            path: '/contactos',
            name: '',
            component: contactos,
        }
    ]
})